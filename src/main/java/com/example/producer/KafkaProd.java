package com.example.producer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaProd {

	
	private static final Logger LOGGER =LoggerFactory.getLogger(KafkaProd.class);
	
	private KafkaTemplate<String, String> kafkatemp;

	public KafkaProd(KafkaTemplate<String, String> kafkatemp) {
		super();
		this.kafkatemp = kafkatemp;
	}
	
	
	public void sendMsg(String message) {
		kafkatemp.send("kafka_tutorials", message);
		LOGGER.info(String.format("Message Sent %s", message));
	}
}

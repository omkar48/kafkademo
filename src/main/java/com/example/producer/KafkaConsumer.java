package com.example.producer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
@Component
public class KafkaConsumer {

public static final	Logger LOGGER = LoggerFactory.getLogger(KafkaConsumer.class);

@KafkaListener(topics="kafka_tutorials",groupId="myGroup")
public void consume(String message) {
	LOGGER.info(String.format("Message Received in Consumer-> %s", message));
}
	
	
}

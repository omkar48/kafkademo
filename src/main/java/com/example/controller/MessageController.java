package com.example.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.producer.KafkaProd;

@RestController
@RequestMapping("/api/kafka")
public class MessageController {

	@Autowired
	private KafkaProd kafkaprod;

	public MessageController(KafkaProd kafkaprod) {
		super();
		this.kafkaprod = kafkaprod;
	}
	
	@GetMapping("/display")
	public ResponseEntity<String> display(@RequestParam("message")String message){
kafkaprod.sendMsg(message);
return ResponseEntity.ok("Message sent successfully " + message);
	}
	
	
	
}

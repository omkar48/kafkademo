package com.example.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.User;
import com.example.producer.JsonKafkaProducer;



@RestController
@RequestMapping ("/jsonkafka")
public class JsonMessageController {

	private JsonKafkaProducer jsonKafkaProducer;

	public JsonMessageController(JsonKafkaProducer jsonKafkaProducer) {
		super();
		this.jsonKafkaProducer = jsonKafkaProducer;
	}
	
	@PostMapping("/json")
	
	public ResponseEntity<String> display(@RequestBody User user){
		jsonKafkaProducer.sendMsh(user);
		return ResponseEntity.ok("Message sent in Json format");
	}

	
	
	
}

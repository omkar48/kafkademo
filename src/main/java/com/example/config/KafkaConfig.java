
package com.example.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;


@Configuration
public class KafkaConfig {

	@Bean
	public NewTopic javaTopic() {
		return TopicBuilder.name("kafka_tutorials").build();
	}

	@Bean
	public NewTopic javaJsonTopic() {
		return TopicBuilder.name("kafka_json").build();
	}








}
